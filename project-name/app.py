from .mod import extra

class Application():
    def __init__(self):
        self.title = "Example application"

    def run(self):
        print("The {} is running".format(self.title))

        ex = extra.Extras()
        print("The {} is also loaded".format(ex.title))

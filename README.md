# Python Basic Project Template

[![Made for Python](https://forthebadge.com/images/badges/made-with-python.svg)](https://www.python.org/)

Plain and simple Python project folder structure template.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Quick Start

Clone the repository with the name of your new project:  
`git clone git@bitbucket.org:Skerwe/python-basic-project-template.git <project-name>`

1. In bash/terminal/command line, `cd` into your project directory.
2. Run `pipenv install` to install from the Pipfile.
3. When it's done installing, run `pipenv shell` to activate the Pipenv shell.
4. The application can be run as a module with `python -m project-name`

## Folder Structure

- [Structuring Your Project](https://docs.python-guide.org/writing/structure/)

## Virtual Environment

- [Pipenv](https://pipenv.pypa.io/en/latest/)

## Testing

TODO

## License

The source code is free -- see the [UNLICENSE](UNLICENSE) file for details
